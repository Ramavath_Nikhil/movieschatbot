package moviechatbot.myapplication.Uber.Adapters;

/**
 * Created by nikhilramavath on 22/06/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Uber.Models.Prices;


public class UberAvailableRidesAdapter extends RecyclerView.Adapter<UberAvailableRidesAdapter.ViewHolder> {
    private Context mContext;
    private List<Prices> mDataSet;

    public UberAvailableRidesAdapter(Context context, List<Prices> list) {
        mContext = context;
        mDataSet = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView tvCabType, tvPriceRange, tvCabTime;
        public ImageView ivCabIcon;

        public ViewHolder(View v) {
            super(v);
            // Get the widget reference from the custom layout
            tvCabTime = (TextView) v.findViewById(R.id.tvCabTime);
            tvPriceRange = (TextView) v.findViewById(R.id.tvPriceRange);
            tvCabType = (TextView) v.findViewById(R.id.tvCabType);
            ivCabIcon = (ImageView) v.findViewById(R.id.ivCabIcon);
        }
    }

    @Override
    public UberAvailableRidesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_ola_uber_cab, parent, false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final Prices prices = mDataSet.get(position);
        Log.d("name", prices.getEstimate());

        long minute = TimeUnit.SECONDS.toMinutes(prices.getDuration()) - (TimeUnit.SECONDS.toHours(prices.getDuration()) * 60);

        if (minute == 1)
            holder.tvCabTime.setText(minute + " min");
        else
            holder.tvCabTime.setText(minute + " mins");


        holder.tvCabType.setText(prices.getLocalized_display_name());
        holder.tvPriceRange.setText(prices.getEstimate());
        holder.ivCabIcon.setImageResource(R.drawable.uber_logo);
        //Glide.with(mContext).load(prices.get)

    }


    @Override
    public int getItemCount() {
        // Count the items
        return mDataSet.size();
    }
}
