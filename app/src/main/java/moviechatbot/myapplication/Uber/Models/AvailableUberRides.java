package moviechatbot.myapplication.Uber.Models;

import java.util.List;

/**
 * Created by nikhilramavath on 22/06/17.
 */

public class AvailableUberRides {

    public List<Prices> getPrices() {
        return prices;
    }

    public void setPrices(List<Prices> prices) {
        this.prices = prices;
    }

    private List<Prices> prices;
}
