package moviechatbot.myapplication.Ola.Models;

import java.util.List;

/**
 * Created by nikhilramavath on 22/06/17.
 */

public class OlaAvailableRideResponse {

    private List<Categories> categories;
    private List<RideEstimates> ride_estimate;

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public List<RideEstimates> getRideEstimates() {
        return ride_estimate;
    }

    public void setRideEstimates(List<RideEstimates> rideEstimates) {
        this.ride_estimate = rideEstimates;
    }
}
