package moviechatbot.myapplication.Ola.Models;

/**
 * Created by nikhilramavath on 22/06/17.
 */

public class FareBrakeUp {

    private String type;
    private String minimum_distance;
    private String minimum_time;
    private String base_fare;
    private String minimum_fare;
    private String cost_per_distance;
    private String waiting_cost_per_minute;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinimum_distance() {
        return minimum_distance;
    }

    public void setMinimum_distance(String minimum_distance) {
        this.minimum_distance = minimum_distance;
    }

    public String getMinimum_time() {
        return minimum_time;
    }

    public void setMinimum_time(String minimum_time) {
        this.minimum_time = minimum_time;
    }

    public String getBase_fare() {
        return base_fare;
    }

    public void setBase_fare(String base_fare) {
        this.base_fare = base_fare;
    }

    public String getMinimum_fare() {
        return minimum_fare;
    }

    public void setMinimum_fare(String minimum_fare) {
        this.minimum_fare = minimum_fare;
    }

    public String getCost_per_distance() {
        return cost_per_distance;
    }

    public void setCost_per_distance(String cost_per_distance) {
        this.cost_per_distance = cost_per_distance;
    }

    public String getWaiting_cost_per_minute() {
        return waiting_cost_per_minute;
    }

    public void setWaiting_cost_per_minute(String waiting_cost_per_minute) {
        this.waiting_cost_per_minute = waiting_cost_per_minute;
    }

    public String getRide_cost_per_minute() {
        return ride_cost_per_minute;
    }

    public void setRide_cost_per_minute(String ride_cost_per_minute) {
        this.ride_cost_per_minute = ride_cost_per_minute;
    }

    public boolean isRates_higher_than_usual() {
        return rates_higher_than_usual;
    }

    public void setRates_higher_than_usual(boolean rates_higher_than_usual) {
        this.rates_higher_than_usual = rates_higher_than_usual;
    }

    public String[] getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String[] surcharge) {
        this.surcharge = surcharge;
    }

    private String ride_cost_per_minute;
    private boolean rates_higher_than_usual;
    private String[] surcharge;
}
