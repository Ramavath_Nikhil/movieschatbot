package moviechatbot.myapplication.Ola.Models;

import java.util.List;

/**
 * Created by nikhilramavath on 22/06/17.
 */

public class Categories {


    private String id,
            display_name,
            currency,
            distance_unit,
            time_unit,
            distance;
    private int eta;

    private List<FareBrakeUp> fare_breakup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDistance_unit() {
        return distance_unit;
    }

    public void setDistance_unit(String distance_unit) {
        this.distance_unit = distance_unit;
    }

    public String getTime_unit() {
        return time_unit;
    }

    public void setTime_unit(String time_unit) {
        this.time_unit = time_unit;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public boolean isRide_later_enabled() {
        return ride_later_enabled;
    }

    public void setRide_later_enabled(boolean ride_later_enabled) {
        this.ride_later_enabled = ride_later_enabled;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private boolean ride_later_enabled;
    private String image;

    public List<FareBrakeUp> getFareBrakeUpList() {
        return fare_breakup;
    }

    public void setFareBrakeUpList(List<FareBrakeUp> fareBrakeUpList) {
        this.fare_breakup = fareBrakeUpList;
    }

    public List<FareBrakeUp> getFare_breakup() {
        return fare_breakup;
    }

    public void setFare_breakup(List<FareBrakeUp> fare_breakup) {
        this.fare_breakup = fare_breakup;
    }
}
