package moviechatbot.myapplication.Ola.Models;

/**
 * Created by mayurlathkar on 22/06/17.
 */

public class RideEstimates {
    private String category, travel_time_in_minutes;
    private float distance;
    private int amount_min, amount_max;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTravel_time_in_minutes() {
        return travel_time_in_minutes;
    }

    public void setTravel_time_in_minutes(String travel_time_in_minutes) {
        this.travel_time_in_minutes = travel_time_in_minutes;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getAmount_min() {
        return amount_min;
    }

    public void setAmount_min(int amount_min) {
        this.amount_min = amount_min;
    }

    public int getAmount_max() {
        return amount_max;
    }

    public void setAmount_max(int amount_max) {
        this.amount_max = amount_max;
    }
}
