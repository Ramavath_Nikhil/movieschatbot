package moviechatbot.myapplication.Models;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class Genre {

    private  int id ;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
