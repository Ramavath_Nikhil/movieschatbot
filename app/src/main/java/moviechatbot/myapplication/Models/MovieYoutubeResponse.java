package moviechatbot.myapplication.Models;

import java.util.ArrayList;

/**
 * Created by nikhilramavath on 21/06/17.
 */

public class MovieYoutubeResponse {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<MovieYoutube> getResults() {
        return results;
    }

    public void setResults(ArrayList<MovieYoutube> results) {
        this.results = results;
    }

    private ArrayList<MovieYoutube> results;
}
