package moviechatbot.myapplication.Models;

/**
 * Created by nikhilramavath on 21/06/17.
 */

public class ChatBotResponseType {

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String intent;
    private String value;
}
