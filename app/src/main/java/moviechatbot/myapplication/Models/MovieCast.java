package moviechatbot.myapplication.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class MovieCast implements Parcelable {

    private int cast_id,gender,id,order;
    private String character;
    private String credit_id;
    private String name;

    protected MovieCast(Parcel in) {
        cast_id = in.readInt();
        gender = in.readInt();
        id = in.readInt();
        order = in.readInt();
        character = in.readString();
        credit_id = in.readString();
        name = in.readString();
        profile_path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cast_id);
        dest.writeInt(gender);
        dest.writeInt(id);
        dest.writeInt(order);
        dest.writeString(character);
        dest.writeString(credit_id);
        dest.writeString(name);
        dest.writeString(profile_path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieCast> CREATOR = new Creator<MovieCast>() {
        @Override
        public MovieCast createFromParcel(Parcel in) {
            return new MovieCast(in);
        }

        @Override
        public MovieCast[] newArray(int size) {
            return new MovieCast[size];
        }
    };

    public int getCast_id() {
        return cast_id;
    }

    public void setCast_id(int cast_id) {
        this.cast_id = cast_id;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    private String profile_path;
}
