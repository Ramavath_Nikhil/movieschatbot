package moviechatbot.myapplication.Models;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Nikil on 1/26/2017.
 */
public class Message {



    private String user,message,createdAt;
    private LatLng pickUpLatLong, dropLatLng;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    private List<Movie> movies;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUser() {

        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LatLng getPickUpLatLong() {
        return pickUpLatLong;
    }

    public void setPickUpLatLong(LatLng latLng) {
        this.pickUpLatLong = latLng;
    }

    public LatLng getDropLatLng() {
        return dropLatLng;
    }

    public void setDropLatLng(LatLng dropLatLng) {
        this.dropLatLng = dropLatLng;
    }
}
