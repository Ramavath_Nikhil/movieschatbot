package moviechatbot.myapplication.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class MovieCrew implements Parcelable {

    private int gender,id;
    private String credit_id,department,job,name,profile_path;

    protected MovieCrew(Parcel in) {
        gender = in.readInt();
        id = in.readInt();
        credit_id = in.readString();
        department = in.readString();
        job = in.readString();
        name = in.readString();
        profile_path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(gender);
        dest.writeInt(id);
        dest.writeString(credit_id);
        dest.writeString(department);
        dest.writeString(job);
        dest.writeString(name);
        dest.writeString(profile_path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieCrew> CREATOR = new Creator<MovieCrew>() {
        @Override
        public MovieCrew createFromParcel(Parcel in) {
            return new MovieCrew(in);
        }

        @Override
        public MovieCrew[] newArray(int size) {
            return new MovieCrew[size];
        }
    };

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public static Creator<MovieCrew> getCREATOR() {
        return CREATOR;
    }
}
