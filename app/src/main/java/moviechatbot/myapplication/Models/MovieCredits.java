package moviechatbot.myapplication.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class MovieCredits implements Parcelable {

    protected MovieCredits(Parcel in) {
        cast = in.createTypedArrayList(MovieCast.CREATOR);
        crew = in.createTypedArrayList(MovieCrew.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(cast);
        dest.writeTypedList(crew);
    }

    public static Creator<MovieCredits> getCREATOR() {
        return CREATOR;
    }

    public ArrayList<MovieCrew> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<MovieCrew> crew) {
        this.crew = crew;
    }

    @Override
    public int describeContents() {

        return 0;
    }

    public static final Creator<MovieCredits> CREATOR = new Creator<MovieCredits>() {
        @Override
        public MovieCredits createFromParcel(Parcel in) {
            return new MovieCredits(in);
        }

        @Override
        public MovieCredits[] newArray(int size) {
            return new MovieCredits[size];
        }
    };

    public ArrayList<MovieCast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<MovieCast> cast) {
        this.cast = cast;
    }

    private ArrayList<MovieCast> cast;
    private ArrayList<MovieCrew> crew;
}
