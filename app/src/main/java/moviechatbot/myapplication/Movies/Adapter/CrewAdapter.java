package moviechatbot.myapplication.Movies.Adapter;

/**
 * Created by nikhilramavath on 03/07/17.
 */

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.Models.MovieCrew;
import moviechatbot.myapplication.R;


public class CrewAdapter extends RecyclerView.Adapter<CrewAdapter.ViewHolder> {
    private Context mContext;
    private List<MovieCrew> mDataSet;

    public CrewAdapter(Context context, List<MovieCrew> list) {
        mContext = context;
        mDataSet = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView tv_role, tv_name;
        public ImageView iv_thumbnail;

        public ViewHolder(View v) {
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.card_view);
            tv_role = (TextView) v.findViewById(R.id.tv_role);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            iv_thumbnail = (ImageView) v.findViewById(R.id.iv_thumbnail);

        }
    }

    @Override
    public CrewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item_cast, parent, false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Get the current color from the data set
        MovieCrew movieCrew = mDataSet.get(position);
        holder.tv_role.setText(movieCrew.getJob());
        holder.tv_name.setText(movieCrew.getName());

        if(movieCrew.getProfile_path()!=null)
        Glide.with(mContext).load(Constants.API_THEMOVIEDB_IMAGE_URL + movieCrew.getProfile_path().substring(1, movieCrew.getProfile_path().length())).into(holder.iv_thumbnail);


        // Set a click listener for CardView
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext,"")
            }
        });
    }

    @Override
    public int getItemCount() {
        // Count the items
        return mDataSet.size();
    }
}