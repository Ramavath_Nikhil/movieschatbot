package moviechatbot.myapplication.Movies.Adapter;

/**
 * Created by nikhilramavath on 03/07/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import moviechatbot.myapplication.Models.MovieYoutube;
import moviechatbot.myapplication.R;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.MyViewHolder> {

    private List<MovieYoutube> moviesList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title,tv_site;
        public ImageView iv_thumbnail;
        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_site = (TextView) view.findViewById(R.id.tv_site);
            iv_thumbnail = (ImageView) view.findViewById(R.id.iv_thumbnail);
        }
    }


    public VideosAdapter(List<MovieYoutube> moviesList,Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_youtube_videos, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MovieYoutube movieYoutube = moviesList.get(position);
        Log.d("video","https://img.youtube.com/vi/"+movieYoutube.getId()+"/default.jpg");
        Glide.with(context).load("https://img.youtube.com/vi/"+movieYoutube.getKey()+"/default.jpg").into(holder.iv_thumbnail);
         holder.tv_site.setText("Source: "+movieYoutube.getSite());
         holder.tv_title.setText(movieYoutube.getName());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}