package moviechatbot.myapplication.Movies.Adapter;

/**
 * Created by nikhilramavath on 03/07/17.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import moviechatbot.myapplication.Models.MovieReview;
import moviechatbot.myapplication.R;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private List<MovieReview> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_author,tv_content;

        public MyViewHolder(View view) {
            super(view);
            tv_author = (TextView) view.findViewById(R.id.tv_author);
            tv_content = (TextView) view.findViewById(R.id.tv_content);
        }
    }


    public ReviewsAdapter(List<MovieReview> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MovieReview movieReview = moviesList.get(position);

        holder.tv_author.setText(movieReview.getAuthor());
        holder.tv_content.setText(movieReview.getContent());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}