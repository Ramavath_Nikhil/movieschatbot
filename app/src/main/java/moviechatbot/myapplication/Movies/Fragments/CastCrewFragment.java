package moviechatbot.myapplication.Movies.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import moviechatbot.myapplication.Models.MovieCast;
import moviechatbot.myapplication.Models.MovieCrew;
import moviechatbot.myapplication.Movies.Adapter.CastAdapter;
import moviechatbot.myapplication.Movies.Adapter.CrewAdapter;
import moviechatbot.myapplication.R;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class CastCrewFragment extends Fragment {

    private Context mContext;
    private Activity mActivity;

    private RelativeLayout mRelativeLayout;
    private RecyclerView mRecyclerView, recycler_view_crew;
    private RecyclerView.LayoutManager mLayoutManager, mCrewLayoutManager;
    private RecyclerView.Adapter mAdapter, mCrewAdapter;
    private List<MovieCast> movieCasts = new ArrayList<>();
    private List<MovieCrew> movieCrews = new ArrayList<>();

    public CastCrewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();
        mActivity = getActivity();
        Bundle arguments = getArguments();
        movieCasts = arguments.getParcelableArrayList("cast");
        movieCrews = arguments.getParcelableArrayList("crew");
    }


    public void elementsIntilization(View view) {
        // Get the widgets reference from XML layout
        mRelativeLayout = (RelativeLayout) view.findViewById(R.id.rl);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recycler_view_crew = (RecyclerView) view.findViewById(R.id.recycler_view_crew);

        // Specify a layout for RecyclerView
        // Create a horizontal RecyclerView
        mLayoutManager = new LinearLayoutManager(
                mContext,
                LinearLayoutManager.HORIZONTAL,
                false
        );

        mCrewLayoutManager = new LinearLayoutManager(
                mContext,
                LinearLayoutManager.HORIZONTAL,
                false
        );


        mRecyclerView.setLayoutManager(mLayoutManager);
        recycler_view_crew.setLayoutManager(mCrewLayoutManager);


        if (movieCasts != null) {
            // Initialize a new Adapter for RecyclerView
            mAdapter = new CastAdapter(mContext, movieCasts);

            // Set an adapter for RecyclerView
            mRecyclerView.setAdapter(mAdapter);
        }


        if (movieCrews != null) {
            // Initialize a new Adapter for RecyclerView
            mCrewAdapter = new CrewAdapter(mContext, movieCrews);

            // Set an adapter for RecyclerView
            recycler_view_crew.setAdapter(mCrewAdapter);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.layout_fragment_cast_crew, container, false);
        elementsIntilization(view);
        return view;
    }
}
