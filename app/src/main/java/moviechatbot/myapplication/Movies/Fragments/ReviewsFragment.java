package moviechatbot.myapplication.Movies.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import moviechatbot.myapplication.Models.MovieReview;
import moviechatbot.myapplication.Movies.Adapter.ReviewsAdapter;
import moviechatbot.myapplication.R;

/**
 * Created by nikhilramavath on 03/07/17.
 */

public class ReviewsFragment extends Fragment {

    private ArrayList<MovieReview> movieReviewList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ReviewsAdapter mAdapter;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_fragment_movie_reviews, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        Bundle arguments = getArguments();
        movieReviewList = arguments.getParcelableArrayList("reviews");

       if(movieReviewList!=null && movieReviewList.size()>0) {
           mAdapter = new ReviewsAdapter(movieReviewList);
           RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
           recyclerView.setLayoutManager(mLayoutManager);
           recyclerView.setItemAnimator(new DefaultItemAnimator());
           recyclerView.setAdapter(mAdapter);
       }
        return view;
    }
}
