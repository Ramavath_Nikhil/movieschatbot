package moviechatbot.myapplication.Activites;

import android.Manifest;
import android.app.Dialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Locale;

import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.android.AIService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.ui.AIButton;
import moviechatbot.myapplication.Adapters.ChatRoomThreadAdapter;
import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.Models.ChatBotResponseType;
import moviechatbot.myapplication.Models.Message;
import moviechatbot.myapplication.Models.MovieSuggestions;
import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Utils.RestClient;
import moviechatbot.myapplication.Views.SoftKeyboard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    private String chatRoomId;
    private RecyclerView recyclerView;
    private ChatRoomThreadAdapter mAdapter;
    private ArrayList<Message> messageArrayList;
    private EditText inputMessage;
    private EditText source, destination;
    private Button btnSend;
    private LatLng pickupLatLng = null, dropLatLng = null;
    private AIButton aiButton;
    private AIConfiguration config;
    private TextView AiResult;
    private AIService aiService;
    private AIDataService aiDataService;
    private Button clickMe;
    private EditText queryEditext;
    private ImageView mic;
    SoftKeyboard softKeyboard;


    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int REQ_CODE_SPEECH_INPUT = 2;
    private static final int PLACES_SOURCE_REQUEST = 3;
    private static final int PLACES_DESTINTION_REQUEST = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        elemetnsIntilization();
        onClickListeners();
    }


    public void elemetnsIntilization() {
        inputMessage = (EditText) findViewById(R.id.message);
        btnSend = (Button) findViewById(R.id.btn_send);
        mic = (ImageView) findViewById(R.id.mic);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        messageArrayList = new ArrayList<>();
        mAdapter = new ChatRoomThreadAdapter(this, messageArrayList, "1");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        config = new AIConfiguration(Constants.API_AI_ACCESS_TOKEN,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);

        Message message = new Message();
        message.setMessage("Welcome Bro, what u want me to speak about?");
        message.setUser("1");
        messageArrayList.add(message);
        mAdapter.notifyDataSetChanged();


        inputMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //got focus
                    mic.setVisibility(View.VISIBLE);
                } else {
                    //lost focus
                    mic.setVisibility(View.GONE);
                }
            }
        });

        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main); // You must use your root layout
        InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);

/*
Instantiate and pass a callback
*/

        softKeyboard = new SoftKeyboard(mainLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

            @Override
            public void onSoftKeyboardHide() {
                // Code here

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mic.setVisibility(View.GONE);

                    }
                });
            }

            @Override
            public void onSoftKeyboardShow() {
                // Code here
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mic.setVisibility(View.VISIBLE);

                    }
                });
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        softKeyboard.unRegisterSoftKeyboardCallback();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        inputMessage.clearFocus();
        mic.setVisibility(View.GONE);
    }

    public void onClickListeners() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inputMessage.getText().toString().isEmpty()) {
                    Toast.makeText(ChatActivity.this, "Please enter some message", Toast.LENGTH_SHORT).show();
                } else {
                    softKeyboard.closeSoftKeyboard();
                    Message message = new Message();
                    message.setMessage(inputMessage.getText().toString());
                    message.setUser("2");
                    messageArrayList.add(message);
                    mAdapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                    AsyncTaskRunner runner = new AsyncTaskRunner();
                    runner.execute(inputMessage.getText().toString());


                }
            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ChatActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_REQUEST_CODE);

                    return;
                }

                promptSpeechInput();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    promptSpeechInput();

                } else {

                    // Snackbar.make(view,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                    Toast.makeText(ChatActivity.this, "Please provide us permission to access your location", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * @author Prabu
     *         Private class which runs the long operation. ( Sleeping for some time )
     */
    private class AsyncTaskRunner extends AsyncTask<String, String, ChatBotResponseType> {

        private ChatBotResponseType resp;

        @Override
        protected ChatBotResponseType doInBackground(String... params) {

            try {
                aiDataService = new AIDataService(ChatActivity.this, config);
                AIRequest aiRequest = new AIRequest();
                aiRequest.setQuery(params[0]);
                //aiDataService.textRequest(aiRequest);
                final AIResponse aiResponse = aiDataService.request(aiRequest);


                if (aiResponse != null) {
                    if (aiResponse.getResult() != null && aiResponse.getResult().getAction().equals(Constants.INTENT_WATHCMOVIETRAILERWITHNAMWE)) {


                        ChatBotResponseType chatBotResponseType = new ChatBotResponseType();
                        chatBotResponseType.setIntent(Constants.INTENT_WATHCMOVIETRAILERWITHNAMWE);
                        chatBotResponseType.setValue(aiResponse.getResult().getParameters().get("any").getAsString());
                        return chatBotResponseType;
                    } else if (aiResponse.getResult() != null && aiResponse.getResult().getAction().equals(Constants.INTENT_BOOKACAB)) {


                        ChatBotResponseType chatBotResponseType = new ChatBotResponseType();
                        chatBotResponseType.setIntent(Constants.INTENT_BOOKACAB);
                        chatBotResponseType.setValue(null);

                        return chatBotResponseType;


                    }

                    else if(aiResponse.getResult() != null && aiResponse.getResult().getAction().equals(Constants.INTENT_UNKNOWN))
                    {
                        ChatBotResponseType chatBotResponseType = new ChatBotResponseType();
                        chatBotResponseType.setIntent(Constants.INTENT_UNKNOWN);
                        chatBotResponseType.setValue(aiResponse.getResult().getResolvedQuery());
                        return chatBotResponseType;
                    }


                    else {
                        if (aiResponse != null && aiResponse.getResult() != null && aiResponse.getResult().getFulfillment() != null && aiResponse.getResult().getFulfillment().getSpeech() != null && !aiResponse.getResult().getFulfillment().getSpeech().isEmpty()) {

                            ChatBotResponseType chatBotResponseType = new ChatBotResponseType();
                            chatBotResponseType.setIntent("Nothing");
                            chatBotResponseType.setValue(aiResponse.getResult().getFulfillment().getSpeech());
                            return chatBotResponseType;
                        } else
                            return resp;
                    }

                } else {
                    return resp;
                }


            } catch (Exception e) {
                return resp;
            }


        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(ChatBotResponseType result) {
            // execution of result of Long time consuming operation


            if (result != null && result.getIntent().equals(Constants.INTENT_WATHCMOVIETRAILERWITHNAMWE)) {

                 fetchMovieSuggestions(result.getValue(),result.getIntent());

            } else if (result != null && result.getIntent().equals(Constants.INTENT_BOOKACAB)) {

                Message aimessage = new Message();
                aimessage.setMessage(getString(R.string.please_provide_source_destination));
                aimessage.setUser("1");
                messageArrayList.add(aimessage);
                mAdapter.notifyDataSetChanged();
                inputMessage.setText("");
                recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                showCabSourceAndDestiantionDialog();

            }
            else if(result!=null && result.getIntent().equals(Constants.INTENT_UNKNOWN))
            {

              fetchMovieSuggestions(result.getValue(),result.getIntent());

            }


            else {


               if(result!=null && result.getValue()!=null) {
                   Message aimessage = new Message();
                   aimessage.setMessage(result.getValue());
                   aimessage.setUser("1");
                   messageArrayList.add(aimessage);
                   mAdapter.notifyDataSetChanged();
                   inputMessage.setText("");
                   recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

               }

                //Toast.makeText(ChatActivity.this, "Unable to fetch result", Toast.LENGTH_SHORT).show();
            }

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    // txtSpeechInput.setText(result.get(0));
                    inputMessage.setText(result.get(0));
                }
                break;
            }

            case PLACES_SOURCE_REQUEST: {
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    if (pickupLatLng == null) {
                        pickupLatLng = place.getLatLng();
                        Log.d("pickupLatLng", place.getLatLng().toString());
                    }
                    source.setText(place.getName());
                    source.setTag(place.getLatLng().latitude + "," + place.getLatLng().longitude);

                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    Toast.makeText(ChatActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case PLACES_DESTINTION_REQUEST: {
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    if (dropLatLng == null) {
                        dropLatLng = place.getLatLng();
                        Log.d("pickupLatLng", place.getLatLng().toString());
                    }
                    destination.setText(place.getName());
                    destination.setTag(place.getLatLng().latitude + "," + place.getLatLng().longitude);
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    Toast.makeText(ChatActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public void fetchMovieSuggestions(String query, final String intent) {


        final Call<MovieSuggestions> call = new RestClient(Constants.API_THEMOVIEDB_BASE_URL).get().getMovieSuggestions(Constants.THE_MOVIEDB_ACCESS_TOKEN, query);
        call.enqueue(new Callback<MovieSuggestions>() {
            @Override
            public void onResponse(Call<MovieSuggestions> call, Response<MovieSuggestions> response) {


                if (response.body()!=null && response.body().getResults().size() > 0) {
                    Log.d("suggestions size", response.body().getResults().size() + "");

                    Message aimessage = new Message();
                    aimessage.setMovies(response.body().getResults());
                    aimessage.setUser("3");
                    messageArrayList.add(aimessage);
                    mAdapter.notifyDataSetChanged();
                    inputMessage.setText("");
                    recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

                } else {



                    if(intent.equals(Constants.INTENT_WATHCMOVIETRAILERWITHNAMWE))
                    {

                        Message aimessage = new Message();
                        aimessage.setMessage("Unable to fetch movie details");
                        aimessage.setUser("1");
                        messageArrayList.add(aimessage);
                        mAdapter.notifyDataSetChanged();
                        inputMessage.setText("");
                        recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

                    }
                    else if (intent.equals(Constants.INTENT_UNKNOWN))
                    {

                        Message aimessage = new Message();
                        aimessage.setMessage("Sorry, I didn't get that.");
                        aimessage.setUser("1");
                        messageArrayList.add(aimessage);
                        mAdapter.notifyDataSetChanged();
                        inputMessage.setText("");
                        recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

                    }
                }




            }

            @Override
            public void onFailure(Call<MovieSuggestions> call, Throwable t) {

                t.printStackTrace();


            }
        });

    }

    public void showCabSourceAndDestiantionDialog() {
        final Dialog dialog = new Dialog(ChatActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_cab_source_destination);

        source = (EditText) dialog.findViewById(R.id.source);
        destination = (EditText) dialog.findViewById(R.id.destiantion);
        Button submit = (Button) dialog.findViewById(R.id.submit);

        source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlacesIntent(PLACES_SOURCE_REQUEST);
            }
        });
        destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlacesIntent(PLACES_DESTINTION_REQUEST);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message aimessage = new Message();
                aimessage.setMessage("Source: "+source.getText()+"( "+source.getTag()+" )\nDestination: "+destination.getText()+"( "+destination.getTag()+" )");
                aimessage.setUser("2");
                messageArrayList.add(aimessage);
                mAdapter.notifyDataSetChanged();
                inputMessage.setText("");
                recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


                Message aimessage1 = new Message();
                aimessage1.setMessage("fetching cab details from "+source.getText()+" to "+destination.getText());
                aimessage1.setUser("1");
                messageArrayList.add(aimessage1);
                mAdapter.notifyDataSetChanged();
                inputMessage.setText("");
                recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


                Message aimessage2 = new Message();
                aimessage2.setMessage("fetching cab details from "+source.getText()+" to "+destination.getText());
                aimessage2.setUser("4");
                if (pickupLatLng != null)
                aimessage2.setPickUpLatLong(pickupLatLng);
                if (dropLatLng != null)
                aimessage2.setDropLatLng(dropLatLng);
                messageArrayList.add(aimessage2);
                mAdapter.notifyDataSetChanged();
                inputMessage.setText("");
                recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


                dialog.cancel();

            }
        });



        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }


    public void callPlacesIntent(int request) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, request);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    public void refreshAdapter()
    {
        mAdapter.notifyDataSetChanged();
    }


}
