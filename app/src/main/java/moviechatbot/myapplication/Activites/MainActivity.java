package moviechatbot.myapplication.Activites;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import ai.api.ui.AIButton;
import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.R;


public class MainActivity extends AppCompatActivity implements AIListener {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int REQ_CODE_SPEECH_INPUT = 2;
    private AIButton aiButton;
    private AIConfiguration config;
    private TextView AiResult;
    private AIService aiService;
    private AIDataService aiDataService;
    private Button clickMe;
    private EditText queryEditext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Functions used to intilization the elements
        elementsIntilziation();

        //VIEW ONCLICK LISTENERS
        onCLickListernes();
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO, android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);

            return;
        }
    }

    public void elementsIntilziation() {

        config = new AIConfiguration(Constants.API_AI_ACCESS_TOKEN,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);


        //EDIITEXT INTILIZATION
        queryEditext = (EditText) findViewById(R.id.queryEditext);

        //BUTTON INTILIZATION
        clickMe = (Button) findViewById(R.id.queryListener);

        //TEXTVIEW INTILIZATION
        AiResult = (TextView) findViewById(R.id.result);

        //AI BUTTON INILIZATION
        aiButton = (AIButton) findViewById(R.id.micButton);
        aiButton.initialize(config);
        aiButton.setResultsListener(new AIButton.AIButtonListener() {
            @Override
            public void onResult(AIResponse result) {

                if (result != null && result.getResult() != null) {
                    Log.d("Result", result.getResult().toString());
                    AiResult.setText(result.getResult().getAction());
                } else {
                    Toast.makeText(MainActivity.this, "Unable to fetch the result", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onError(AIError error) {

                Toast.makeText(MainActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                Log.d("error", error.getMessage());

            }

            @Override
            public void onCancelled() {

                Toast.makeText(MainActivity.this, "Request has been cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    // txtSpeechInput.setText(result.get(0));
                }
                break;
            }

        }
    }

    public void onCLickListernes()

    {
        clickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (queryEditext.getText().toString().isEmpty()) {


                    aiService = AIService.getService(MainActivity.this, config);
                    aiService.setListener(MainActivity.this);
                    aiService.startListening();

                } else {

                    Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {

                                aiDataService = new AIDataService(MainActivity.this, config);
                                AIRequest aiRequest = new AIRequest();
                                aiRequest.setQuery(queryEditext.getText().toString());
                                //aiDataService.textRequest(aiRequest);
                                final AIResponse aiResponse = aiDataService.request(aiRequest);
                                if (aiResponse != null && aiResponse.getResult() != null) {
                                    Log.d("result", aiResponse.getResult().getFulfillment().getSpeech());
                                } else {
                                    Log.d("error", "tre");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();
                }


            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    // Snackbar.make(view,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this, "Please provide us permission to access your location", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onResult(AIResponse response) {
       /* if (result != null && result.getResult() != null) {
            Log.d("Result", result.getResult().toString());
            AiResult.setText(result.getResult().getAction());
        } else {
            Toast.makeText(MainActivity.this, "Unable to fetch the result", Toast.LENGTH_SHORT).show();
        }*/

        Result result = response.getResult();

        // Get parameters
        String parameterString = "";
        if (result.getParameters() != null && !result.getParameters().isEmpty()) {
            for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
                parameterString += "(" + entry.getKey() + ", " + entry.getValue() + ") ";
            }


        }

        // Show results in TextView.
        AiResult.setText("Query:" + result.getResolvedQuery() +
                "\nAction: " + result.getAction() +
                "\nParameters: " + parameterString + "\nresult: " + response.getResult().getFulfillment().getSpeech());


    }

    @Override
    public void onError(AIError error) {

        Toast.makeText(MainActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
        Log.d("error", error.getMessage());
    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }
}
