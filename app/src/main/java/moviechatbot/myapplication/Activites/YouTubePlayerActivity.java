package moviechatbot.myapplication.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.pedrovgs.DraggableListener;
import com.github.pedrovgs.DraggablePanel;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.Fragments.YoutubeFragment;
import moviechatbot.myapplication.Models.Genre;
import moviechatbot.myapplication.Models.Movie;
import moviechatbot.myapplication.Models.MovieCast;
import moviechatbot.myapplication.Movies.Fragments.CastCrewFragment;
import moviechatbot.myapplication.Movies.Fragments.ReviewsFragment;
import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Utils.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.util.Preconditions.checkArgument;


/**
 * Sample activity created to show a video from YouTube using a YouTubePlayer.
 *
 * @author Pedro Vicente Gómez Sánchez.
 */
public class YouTubePlayerActivity extends FragmentActivity {


    ImageView thumbnailImageView;
    DraggablePanel draggablePanel;

    private YouTubePlayer youtubePlayer;
    private YouTubePlayerSupportFragment youtubeFragment;


    private Movie movie;
    private TextView tv_title, tv_rating, tv_genre, tv_releasedate, tv_description;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube_player);

        elementsIntilization();
        onClickListeners();
    }

    public void elementsIntilization() {

        thumbnailImageView = (ImageView) findViewById(R.id.iv_thumbnail);



        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_genre = (TextView) findViewById(R.id.tv_genres);
        tv_releasedate = (TextView) findViewById(R.id.tv_relase_date);
        tv_rating = (TextView) findViewById(R.id.tv_rating);
        tv_description = (TextView) findViewById(R.id.tv_description);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);


        Intent intent = getIntent();
        if (intent != null) {
            fetchVideos(intent.getStringExtra("movie_id"));

        }
    }

    public void onClickListeners()
    {
        thumbnailImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(draggablePanel==null)
                {
                    initializeYoutubeFragment();
                    initializeDraggablePanel();
                    hookDraggablePanelListeners();
                }
                else

                draggablePanel.maximize();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {


        Bundle bundle = new Bundle();
        ArrayList<MovieCast> casts = movie.getCredits().getCast();
        bundle.putParcelableArrayList("cast", casts);
        bundle.putParcelableArrayList("crew", movie.getCredits().getCrew());
        CastCrewFragment castCrewFragment = new CastCrewFragment();
        castCrewFragment.setArguments(bundle);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(castCrewFragment, "CAST & CREW");


        bundle = new Bundle();
        bundle.putParcelableArrayList("reviews", movie.getReviews().getResults());
        ReviewsFragment reviewsFragment = new ReviewsFragment();
        reviewsFragment.setArguments(bundle);
        adapter.addFragment(reviewsFragment, "REVIEWS");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void initializeYoutubeFragment() {
        draggablePanel = (DraggablePanel) findViewById(R.id.draggable_panel);
        youtubeFragment = new YouTubePlayerSupportFragment();
        youtubeFragment.initialize(Constants.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {


                    youtubePlayer = player;
                    youtubePlayer.loadVideo(movie.getVideos().getResults().get(0).getKey());
                    youtubePlayer.setShowFullscreenButton(true);

                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                YouTubeInitializationResult error) {
            }
        });
    }

    /**
     * Initialize and configure the DraggablePanel widget with two fragments and some attributes.
     */
    private void initializeDraggablePanel() {
        draggablePanel.setFragmentManager(getSupportFragmentManager());
        draggablePanel.setTopFragment(youtubeFragment);
        YoutubeFragment moviePosterFragment = new YoutubeFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("videos",movie.getVideos().getResults());
        moviePosterFragment.setArguments(bundle);
        draggablePanel.setBottomFragment(moviePosterFragment);
        draggablePanel.initializeView();





    }


    private void hookDraggablePanelListeners() {
        draggablePanel.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                playVideo();
            }

            @Override
            public void onMinimized() {
                //Empty
            }

            @Override
            public void onClosedToLeft() {
                pauseVideo();
            }

            @Override
            public void onClosedToRight() {
                pauseVideo();
            }
        });
    }

    /**
     * Pause the video reproduced in the YouTubePlayer.
     */
    private void pauseVideo() {
        if (youtubePlayer.isPlaying()) {
            youtubePlayer.pause();
        }
    }

    /**
     * Resume the video reproduced in the YouTubePlayer.
     */
    private void playVideo() {
        if (youtubePlayer!=null && !youtubePlayer.isPlaying()) {
            youtubePlayer.play();
        }
    }


    private void printMovieDetails() {
        if (movie != null) {
            tv_title.setText(movie.getOriginalTitle());
            tv_rating.setText(movie.getVoteAverage() * 10 + " % ( " + movie.getVoteCount() + " Votes )");
            tv_description.setText(movie.getOverview());

            Glide.with(this)
                    .load(Constants.API_THEMOVIEDB_IMAGE_URL + movie.getPosterPath().substring(1, movie.getPosterPath().length()))
                    .into(thumbnailImageView);

            String[] releasedate = movie.getReleaseDate().split("-");
            tv_releasedate.setText(getDayOfMonthSuffix(Integer.parseInt(releasedate[2])) + " " + getMonthStandaloneName(Integer.parseInt(releasedate[1])) + " " + releasedate[0]);

            if (movie.getGenres() != null && movie.getGenres().size() > 0) {
                for (Genre genre : movie.getGenres()) {

                    if (tv_genre.getText().toString().isEmpty()) {
                        tv_genre.setText(genre.getName());
                    } else

                        tv_genre.setText(tv_genre.getText() + ", " + genre.getName());

                }

            } else {
                tv_genre.setText("");
            }
        }
    }


    String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return n + "st";
            case 2:
                return n + "nd";
            case 3:
                return n + "rd";
            default:
                return n + "th";
        }
    }

    public static String getMonthStandaloneName(int monthNumber) {
        return new DateFormatSymbols().getMonths()[monthNumber - 1];
    }


    public void fetchVideos(String movie_id) {
        Call<Movie> call = new RestClient(Constants.API_THEMOVIEDB_BASE_URL).get().getVideos(movie_id, Constants.THE_MOVIEDB_ACCESS_TOKEN, "videos,reviews,credits");
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {

                if (response.body() != null) {
                    movie = response.body();
                    printMovieDetails();


                    setupViewPager(viewPager);
                    tabLayout.setupWithViewPager(viewPager);

                } else {
                    Log.d("response", call.request().url().toString());
                }

            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}