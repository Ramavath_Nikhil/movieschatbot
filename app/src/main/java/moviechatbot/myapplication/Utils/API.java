package moviechatbot.myapplication.Utils;




import moviechatbot.myapplication.Models.Movie;
import moviechatbot.myapplication.Models.MovieSuggestions;
import moviechatbot.myapplication.Ola.Models.OlaAvailableRideResponse;
import moviechatbot.myapplication.Uber.Models.AvailableUberRides;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

;


/**
 * Created by Yogendra Singh at G-Axon on 10/15/2015.
 */
public interface API {


    @GET("search/movie")
    @Headers({"Content-Type: application/json"})
    Call<MovieSuggestions> getMovieSuggestions(@Query("api_key") String api_key,@Query("query") String movie_name);

    @GET("movie/{movie_id}")
    @Headers({"Content-Type: application/json"})
    Call<Movie> getVideos(@Path("movie_id") String movie_id,@Query("api_key") String api_key,@Query("append_to_response") String append_to_response);



    //OLA API CALLS

    @GET("products")
    @Headers({"Content-Type: application/json"})
    Call<OlaAvailableRideResponse> getAvailableRidesWithEstimates(@Header("X-APP-TOKEN") String authorization, @Query("pickup_lat") float pickup_lat, @Query("pickup_lng") float pickup_lng,
                                                                  @Query("drop_lat") float drop_lat, @Query("drop_lng") float drop_lng);


    //UBER API CALLS

    @GET("estimates/price")
    @Headers({"Content-Type: application/json"})
    Call<AvailableUberRides> getAvaiableUberRides(@Header("Authorization") String authorization, @Query("start_latitude") float start_latitude, @Query("start_longitude") float start_longitude,@Query("end_latitude") float end_latitude,@Query("end_longitude") float end_longitude);


}
