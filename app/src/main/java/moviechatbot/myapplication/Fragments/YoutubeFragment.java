package moviechatbot.myapplication.Fragments;

/**
 * Created by nikhilramavath on 30/06/17.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import moviechatbot.myapplication.Models.MovieYoutube;
import moviechatbot.myapplication.Movies.Adapter.VideosAdapter;
import moviechatbot.myapplication.R;


/**
 * Fragment implementation created to show a poster inside an ImageView widget.
 *
 * @author Pedro Vicente Gómez Sánchez.
 */
public class YoutubeFragment extends Fragment {


    private ArrayList<MovieYoutube> movieYoutubes = new ArrayList<>();
    private RecyclerView recyclerView;
    private VideosAdapter mAdapter;

    /**
     * Override method used to initialize the fragment.
     */
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_youtube, container, false);

        recyclerView= (RecyclerView) view.findViewById(R.id.recycler_view);
        Bundle arguments = getArguments();
        if(arguments!=null)
        movieYoutubes = arguments.getParcelableArrayList("videos");

        if(movieYoutubes!=null && movieYoutubes.size()>0) {
            mAdapter = new VideosAdapter(movieYoutubes,getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }



}
