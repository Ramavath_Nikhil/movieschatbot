package moviechatbot.myapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Uber.Adapters.UberAvailableRidesAdapter;
import moviechatbot.myapplication.Uber.Models.AvailableUberRides;
import moviechatbot.myapplication.Utils.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mayurlathkar on 22/06/17.
 */

public class UberCabsFragment extends Fragment {

    LatLng pickUp, drop;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        pickUp = getArguments().getParcelable("pick_up");
        drop = getArguments().getParcelable("drop");
        View itemView = inflater.inflate(R.layout.item_ola_uber_list, container, false);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        fetchNearestUberRidesWithEsitmation();

        return itemView;
    }


    public void fetchNearestUberRidesWithEsitmation() {

        Call<AvailableUberRides> call = new RestClient(Constants.API_UBER_BASE_URL).get().getAvaiableUberRides(Constants.UBER_SERVER_TOKEN, (float)pickUp.latitude,(float)pickUp.longitude,(float)drop.latitude,(float)drop.longitude);
        call.enqueue(new Callback<AvailableUberRides>() {
            @Override
            public void onResponse(Call<AvailableUberRides> call, Response<AvailableUberRides> response) {

                if(response.body()!=null) {
                    Log.d("response", response.body().getPrices().size() + "");

                    UberAvailableRidesAdapter mAdapter = new UberAvailableRidesAdapter(getActivity(),response.body().getPrices());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerViewTouchListener();


                }
                else
                {

                }

            }

            @Override
            public void onFailure(Call<AvailableUberRides> call, Throwable t) {

                t.printStackTrace();
            }
        });

    }

    public void recyclerViewTouchListener()
    {
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

    }


}
