package moviechatbot.myapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import moviechatbot.myapplication.Adapters.OlaCabsAdapter;
import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.Ola.Models.OlaAvailableRideResponse;
import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Utils.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mayurlathkar on 22/06/17.
 */

public class OlaCabsFragment extends Fragment {

    RecyclerView recyclerView;
    LatLng pickUp, drop;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pickUp = getArguments().getParcelable("pick_up");
        drop = getArguments().getParcelable("drop");
        View itemView = inflater.inflate(R.layout.item_ola_uber_list, null);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        fetchNearestOlaRidesWithEsitmation();
        return itemView;
    }


    public void fetchNearestOlaRidesWithEsitmation() {

        Call<OlaAvailableRideResponse> call = new RestClient(Constants.API_OLA_BASE_URL).get().getAvailableRidesWithEstimates(Constants.OLA_APP_TOKEN, (float)pickUp.latitude,(float)pickUp.longitude,(float)drop.latitude,(float)drop.longitude);
        call.enqueue(new Callback<OlaAvailableRideResponse>() {
            @Override
            public void onResponse(Call<OlaAvailableRideResponse> call, Response<OlaAvailableRideResponse> response) {

               if(response.body()!=null) {

                   OlaCabsAdapter olaCabsAdapter = new OlaCabsAdapter(getContext(), response.body());
                   recyclerView.setAdapter(olaCabsAdapter);
                   recyclerViewTouchListener();

               }
               else
               {
                   Log.d("response code",response.code()+" "+call.request().url());
               }
            }

            @Override
            public void onFailure(Call<OlaAvailableRideResponse> call, Throwable t) {

                t.printStackTrace();
            }
        });

    }

    public void recyclerViewTouchListener()
    {
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

    }

}
