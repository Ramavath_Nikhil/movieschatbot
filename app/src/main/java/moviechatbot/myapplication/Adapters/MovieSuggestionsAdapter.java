package moviechatbot.myapplication.Adapters;

/**
 * Created by nikhilramavath on 21/06/17.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import moviechatbot.myapplication.Activites.YouTubePlayerActivity;
import moviechatbot.myapplication.Config.Constants;
import moviechatbot.myapplication.Models.Movie;
import moviechatbot.myapplication.R;


public class MovieSuggestionsAdapter extends RecyclerView.Adapter<MovieSuggestionsAdapter.ViewHolder> {
    private Context mContext;
    private List<Movie> mDataSet;

    public MovieSuggestionsAdapter(Context context, List<Movie> list) {
        mContext = context;
        mDataSet = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView mTextView;
        public ImageView mMovieIcon;

        public ViewHolder(View v) {
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.card_view);
            mTextView = (TextView) v.findViewById(R.id.tvMovieName);
            mMovieIcon = (ImageView) v.findViewById(R.id.movieImage);
        }
    }

    @Override
    public MovieSuggestionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item_movie_suggestion_movie, parent, false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final Movie movie = mDataSet.get(position);
        Log.d("movie name ", movie.getOriginalTitle());
        holder.mTextView.setText(movie.getOriginalTitle());
        Glide.with(mContext).load(Constants.API_THEMOVIEDB_IMAGE_URL+movie.getPosterPath()).into(holder.mMovieIcon);
        // Set a click listener for CardView
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                mContext.startActivity(new Intent(mContext, YouTubePlayerActivity.class).putExtra("movie_id", movie.getId()+""));


            }
        });
    }


    @Override
    public int getItemCount() {
        // Count the items
        return mDataSet.size();
    }
}