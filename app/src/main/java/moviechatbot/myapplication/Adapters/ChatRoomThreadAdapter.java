package moviechatbot.myapplication.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import moviechatbot.myapplication.Fragments.OlaCabsFragment;
import moviechatbot.myapplication.Fragments.UberCabsFragment;
import moviechatbot.myapplication.Models.Message;
import moviechatbot.myapplication.R;
import moviechatbot.myapplication.Views.SlidingTabLayout;

/**
 * Created by Nikil on 1/26/2017.
 */
public class ChatRoomThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static String TAG = ChatRoomThreadAdapter.class.getSimpleName();

    private String userId = "1";
    private String moviesId = "3";
    private String fetchCabsId = "4";
    private int SELF = -1;
    private int MOVIESUGGESTIOn = -2;
    private int CABSSUGGESTION = -3;
    private static String today;
    private CabViewPagerAdapter pagerAdapter;
    private LatLng pickUpLatLng = null, dropLatLng = null;
    private Context mContext;
    private ArrayList<Message> messageArrayList;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message, timestamp;

        public ViewHolder(View view) {
            super(view);
            message = (TextView) itemView.findViewById(R.id.message);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
        }
    }


    public class MovieSuggestionViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView recyclerView;

        public MovieSuggestionViewHolder(View view) {
            super(view);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        }
    }

    public class CabsSuggestionsViewHolder extends RecyclerView.ViewHolder {

        ViewPager viewPager;
        SlidingTabLayout tabs;

        public CabsSuggestionsViewHolder(View itemView) {
            super(itemView);
            viewPager = (ViewPager) itemView.findViewById(R.id.vpMenu);
            tabs = (SlidingTabLayout) itemView.findViewById(R.id.tabs);
            tabs.setDistributeEvenly(true);

        }
    }


    public ChatRoomThreadAdapter(Context mContext, ArrayList<Message> messageArrayList, String userId) {
        this.mContext = mContext;
        this.messageArrayList = messageArrayList;
        this.userId = userId;
        this.pagerAdapter = new CabViewPagerAdapter(((AppCompatActivity) mContext).getSupportFragmentManager());
        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        // view type is to identify where to render the chat message
        // left or right
        if (viewType == SELF) {
            // self message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_aichat, parent, false);
            return new ViewHolder(itemView);
        } else if (viewType == MOVIESUGGESTIOn) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_movie_suggestions, parent, false);
            return new MovieSuggestionViewHolder(itemView);
        } else if (viewType == CABSSUGGESTION) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_cabs_suggestions, parent, false);
            return new CabsSuggestionsViewHolder(itemView);
        } else {
            // others message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_chat, parent, false);
            return new ViewHolder(itemView);
        }


    }


    @Override
    public int getItemViewType(int position) {
        Message message = messageArrayList.get(position);
        if (message.getUser().equalsIgnoreCase(userId)) {
            return SELF;
        } else if (message.getUser().equalsIgnoreCase(moviesId)) {
            return MOVIESUGGESTIOn;
        } else if (message.getUser().equalsIgnoreCase(fetchCabsId)) {
            return CABSSUGGESTION;
        }

        return position;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MovieSuggestionViewHolder) {
            Message message = messageArrayList.get(position);
            MovieSuggestionViewHolder movieSuggestionViewHolder = (MovieSuggestionViewHolder) holder;
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(
                    mContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
            );
            movieSuggestionViewHolder.recyclerView.setLayoutManager(mLayoutManager);
            MovieSuggestionsAdapter mAdapter = new MovieSuggestionsAdapter(mContext, message.getMovies());
            movieSuggestionViewHolder.recyclerView.setAdapter(mAdapter);


        } else if (holder instanceof ViewHolder) {
            Message message = messageArrayList.get(position);
            ((ViewHolder) holder).message.setText(message.getMessage());

            // String timestamp = getTimeStamp(message.getCreatedAt());
            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
            String timestamp = df.format(Calendar.getInstance().getTime());

            ((ViewHolder) holder).timestamp.setText(timestamp);

        } else if (holder instanceof CabsSuggestionsViewHolder) {

            CabsSuggestionsViewHolder cabsSuggestionsViewHolder = (CabsSuggestionsViewHolder) holder;
            Message message = messageArrayList.get(position);
            pickUpLatLng = message.getPickUpLatLong();
            dropLatLng = message.getDropLatLng();
            Bundle bundle = new Bundle();
            bundle.putParcelable("pick_up", pickUpLatLng);
            bundle.putParcelable("drop", dropLatLng);

            OlaCabsFragment olaCabsFragment = new OlaCabsFragment();
            olaCabsFragment.setArguments(bundle);

            UberCabsFragment uberCabsFragment = new UberCabsFragment();
            uberCabsFragment.setArguments(bundle);


            pagerAdapter.addFrag(olaCabsFragment, "Ola");
            pagerAdapter.addFrag(uberCabsFragment, "Uber");
            cabsSuggestionsViewHolder.viewPager.setAdapter(pagerAdapter);
            cabsSuggestionsViewHolder.tabs.setViewPager(cabsSuggestionsViewHolder.viewPager);

        }
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;
    }
}
