package moviechatbot.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import moviechatbot.myapplication.Ola.Models.OlaAvailableRideResponse;
import moviechatbot.myapplication.R;

/**
 * Created by mayurlathkar on 22/06/17.
 */

public class OlaCabsAdapter extends RecyclerView.Adapter<OlaCabsAdapter.ViewHolder> {


    private Context mContext;
    private OlaAvailableRideResponse rideResponses;

    public OlaCabsAdapter(Context context, OlaAvailableRideResponse list) {
        this.mContext = context;
        this.rideResponses = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layoutView = inflater.inflate(R.layout.item_ola_uber_cab, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(mContext).load(rideResponses.getCategories().get(position).getImage()).into(holder.ivCabIcon);
        holder.tvCabType.setText(rideResponses.getCategories().get(position).getDisplay_name());

        String etaTimeUnit = rideResponses.getCategories().get(position).getTime_unit();
        if (etaTimeUnit.equalsIgnoreCase("minute") || etaTimeUnit.equalsIgnoreCase("minutes")) {
            if (rideResponses.getCategories().get(position).getEta() == 1)
                holder.tvCabTime.setText(rideResponses.getCategories().get(position).getEta() + " min");
            else
                holder.tvCabTime.setText(rideResponses.getCategories().get(position).getEta() + " mins");

        } else {
            holder.tvCabTime.setText(rideResponses.getCategories().get(position).getEta() + etaTimeUnit);

        }


        if (rideResponses.getRideEstimates() != null && rideResponses.getRideEstimates().size()>0) {
            holder.tvPriceRange.setText(mContext.getString(R.string.Rs)+rideResponses.getRideEstimates().get(0).getAmount_min() + "-" + rideResponses.getRideEstimates().get(0).getAmount_max());
        }

    }

    @Override
    public int getItemCount() {
        return rideResponses.getCategories().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivCabIcon;
        TextView tvCabType, tvPriceRange, tvCabTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ivCabIcon = (ImageView) itemView.findViewById(R.id.ivCabIcon);
            tvCabType = (TextView) itemView.findViewById(R.id.tvCabType);
            tvPriceRange = (TextView) itemView.findViewById(R.id.tvPriceRange);
            tvCabTime = (TextView) itemView.findViewById(R.id.tvCabTime);
        }
    }
}
