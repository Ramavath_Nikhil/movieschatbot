package moviechatbot.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import moviechatbot.myapplication.Ola.Models.OlaAvailableRideResponse;
import moviechatbot.myapplication.R;

/**
 * Created by mayurlathkar on 22/06/17.
 */

public class UberCabsAdapter extends RecyclerView.Adapter<UberCabsAdapter.ViewHolder>{

    private Context mContext;
    private List<OlaAvailableRideResponse> rideResponses;

    public UberCabsAdapter(Context context, List<OlaAvailableRideResponse> list) {
        this.mContext = context;
        this.rideResponses = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layoutView = inflater.inflate(R.layout.item_ola_uber_cab, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return rideResponses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
